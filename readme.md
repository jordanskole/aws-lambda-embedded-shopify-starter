`./tsconfig.json`

```
{
  "compilerOptions": {
    "module": "commonjs",
    "target": "es2017",
    "lib": [
      "es2017",
      "dom",
      "esnext"
    ],
    "moduleResolution": "node",
    "rootDir": "./",
    "sourceMap": true,
    "allowJs": true,
    "esModuleInterop": true,
    "noImplicitAny": true,
    "noUnusedLocals": true,
    "noImplicitThis": true,
    "strictNullChecks": true,
    "noImplicitReturns": true,
    "preserveConstEnums": true,
    "suppressImplicitAnyIndexErrors": true,
    "forceConsistentCasingInFileNames": true
  },
  "exclude": [ "node_modules", ".build", "webpack", ".git" ],
  "types": [ "typePatches" ]
}

```


`./handler.ts`

```
import { Context } from 'aws-lambda';
import { ApolloServer, defaultPlaygroundOptions } from 'apollo-server-lambda';
import { schema as typeDefs } from './schema';
import { resolvers } from './resolvers';

const server = new ApolloServer({
  typeDefs,
  resolvers,
  playground: defaultPlaygroundOptions,
  context: ({ event, context }: { event: any, context: Context}) => ({
    headers: event.headers,
    functionName: context.functionName,
    event,
    context,
  }),
});

const graphql = server.createHandler({
  cors: {
    origin: true,
    credentials: true,
  },
});


export { graphql }
```
