import AWS from 'aws-sdk'

export const docClient = process.env.STAGE === ('dev' || 'local')
  ? new AWS.DynamoDB.DocumentClient({ region: 'localhost', endpoint: 'http://localhost:8000' })
  : new AWS.DynamoDB.DocumentClient()
