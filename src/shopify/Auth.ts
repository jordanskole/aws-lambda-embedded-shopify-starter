import { Callback, Context } from 'aws-lambda'
import { errorResponse, nonce, shopifyInstallUrl, redirectTo, validateRequest } from './utilities'
// TYPES

// CONST
const { SHOPIFY_SECRET, SHOPIFY_KEY, SERVER_BASE_URL, CLIENT_BASE_URL } = process.env;
const redirectUri: string = `${SERVER_BASE_URL}/callback`
const scopes: string = 'read_products,write_products';
const appUrl: string = `${CLIENT_BASE_URL}`;

// HANDLER
const Auth = async (event: any, context: Context, callback: Callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  try {
    const queryString = event.queryStringParameters;
    const { shop: shopName } = queryString;

    // if the shop is already installed
    switch(await validateRequest(queryString, SHOPIFY_SECRET ? SHOPIFY_SECRET : '')) {

      // redirect to the install url
      case 'NOT_INSTALLED':
        const installUrl = shopifyInstallUrl(shopName, SHOPIFY_KEY, scopes, nonce(), redirectUri)
        callback(null, redirectTo(installUrl))
        break;

      // redirect to the application
      case 'AUTHORIZED':
        callback(null, redirectTo(appUrl));
        break;

      default:
        callback(null, redirectTo(appUrl))
        break;
    }

  } catch (err) {
    callback(errorResponse(500, err), null)
  }
}

export { Auth }
