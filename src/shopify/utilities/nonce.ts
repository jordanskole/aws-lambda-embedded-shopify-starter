import { randomBytes } from 'crypto'

export const nonce = () => {
  const n = randomBytes(20).toString('hex');
  // TODO: Save n to memcache or w/e
  return n;
}
