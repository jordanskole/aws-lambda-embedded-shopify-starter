import dynamodb from 'serverless-dynamodb-client'
const docClient = dynamodb.doc;

type ShopIsInstalledArgs = {
  shopName: string;
}

export const shopIsInstalled = async ({ shopName }: ShopIsInstalledArgs) => {
  const params = {
    TableName: 'ShopifyUsers',
    Key: { shopName },
    ConsistentRead: false, // optional (true | false)
    ReturnConsumedCapacity: 'NONE', // optional (NONE | TOTAL | INDEXES)
  };
  // check the database if the shop is installed
  const shopData = await docClient.get(params).promise()
  return shopData.Item ? true : false;
}
