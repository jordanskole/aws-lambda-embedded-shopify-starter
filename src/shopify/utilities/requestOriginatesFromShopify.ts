import { stringify } from 'query-string'
import { createHmac } from 'crypto'

// TYPES

// accepts a shop string and checks the status of the shop
export const requestOriginatesFromShopify = (queryString: any, shopifySecret: string | undefined) => {
  if (!shopifySecret) throw 'shopify secret is required';
  const { hmac } = queryString;
  if (!hmac) {
    throw new Error('hmac parameter is required');
  }

  const map = {...queryString};
  delete map['signature'];
  delete map['hmac'];
  const message = stringify(map);
  const generatedHash = createHmac('sha256', shopifySecret)
    .update(message)
    .digest('hex');

  return hmac === generatedHash;
}
