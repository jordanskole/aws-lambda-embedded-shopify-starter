export const shopifyInstallUrl = (shop: string, shopifyKey: string | undefined, scopes: string, state: string, redirectUri: string) => {  
  return 'https://' + shop +
    '/admin/oauth/authorize?client_id=' + shopifyKey +
    '&scope=' + scopes +
    '&state=' + state +
    '&redirect_uri=' + redirectUri;
}
