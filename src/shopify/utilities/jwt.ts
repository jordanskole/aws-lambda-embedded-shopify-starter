import { sign, verify } from 'jsonwebtoken';

// const { JWT_SECRET } = process.env;
const { JWT_SECRET } = process.env;

export const shopifyAuthToken = (shopDetails: any, jwtSecret: string | undefined = JWT_SECRET) => {
  if (jwtSecret) return sign(shopDetails, jwtSecret);
  throw "process.env.JWT_SECRET is not set";
};

export const verifyShopifyToken = (token: string, jwtSecret: string | undefined = JWT_SECRET) => {
  if (jwtSecret) return verify(token, jwtSecret);
  throw "process.env.JWT_SECRET is not set";
};
