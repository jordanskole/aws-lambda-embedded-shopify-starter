import { registerWebhook } from './registerWebhook'

export const registerUninstallWebhook = async (shop: string, token: string) => {
  return registerWebhook({ shop, token, webhook: 'app/uninstalled'});
}
