import { shopifyAuthToken, verifyShopifyToken } from '../jwt'

const SECRET = 'secret'

const shopName = "testshop.myshopify.com"

it('encodes/decodes a token correctly', () => {
  const token = shopifyAuthToken({shopName}, SECRET)
  const decoded: any = verifyShopifyToken(token, SECRET);
  expect(decoded.shopName).toBe(shopName)
})

it('expects shopifyAuthToken to error without a secret', () => {
  expect(() => shopifyAuthToken({shopName})).toThrow()
})

it('expects verifyShopifyToken to error without a secret', () => {
  expect(() => verifyShopifyToken('token')).toThrow()
})
