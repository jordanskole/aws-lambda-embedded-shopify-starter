import { requestOriginatesFromShopify } from '../requestOriginatesFromShopify'

it('throws if hmac is not supplied', () => {
  expect(() => requestOriginatesFromShopify({ shop: "testshop.myshopify.com"} , 'shhhh')).toThrow()
})

it('returns a boolean', () => {
  expect(requestOriginatesFromShopify({ shop: "testshop.myshopify.com", hmac: '1234'} , 'shhhh')).toBe(false)
})
