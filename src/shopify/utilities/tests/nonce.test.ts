import { nonce } from '../nonce'

it('expects nonce to be a string', () => {
  expect(nonce()).toBeTruthy()
})
