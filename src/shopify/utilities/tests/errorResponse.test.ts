import { errorResponse } from '../errorResponse'

it('properly formats a response', () => {
  expect(errorResponse(400, Error("authentication error"))).toMatchObject({
    "statusCode": 400,
    "body": "{\"name\":\"Error\",\"message\":\"authentication error\"}"
  })
})

it('passes body as a string', () => {
  expect(typeof errorResponse(400, Error("authentication error")).body).toBe("string")
})

it('passes statusCode as an integer', () => {
  expect(typeof errorResponse(400, Error("authentication error")).statusCode).toBe("number")
})
