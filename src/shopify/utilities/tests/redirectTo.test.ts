import { redirectTo } from '../redirectTo'

it('expect redirectTo return a response object', () => {
  expect(redirectTo('https://example.com')).toMatchObject({ statusCode: 302, headers: {Location: 'https://example.com'}, body: null })
})
