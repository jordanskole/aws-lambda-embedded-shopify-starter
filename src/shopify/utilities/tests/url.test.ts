import { shopifyInstallUrl } from '../url'

it('contains a url string', () => {
  expect(shopifyInstallUrl('example.com', 'shopifyKey', 'read_products', 'random', 'https://example.com/callback')).toContain('/admin/oauth/authorize?client_id=')
})
