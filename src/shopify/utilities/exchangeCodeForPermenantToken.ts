// TYPES
type TokenParameters = {
  shopName: string;
  code: string;
  client_id?: string | undefined;
  client_secret?: string | undefined;
}

// type TokenResponse = {
//
// }

// CONST
const { SHOPIFY_SECRET, SHOPIFY_KEY } = process.env;

export const exchangeCodeForPermenantToken = async ({ shopName, code, client_id = SHOPIFY_KEY, client_secret = SHOPIFY_SECRET }: TokenParameters) => {
  if (!client_id) throw "client_id is required as a parameter or set as process.env.SHOPIFY_KEY"
  if (!client_secret) throw "client_secret is required as a parameter or set as process.env.SHOPIFY_KEY"

  const res = await fetch(`https://${shopName}/admin/oauth/access_token`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ client_id, client_secret, code })
  })
  const json = await res.json();
  return json;
}
