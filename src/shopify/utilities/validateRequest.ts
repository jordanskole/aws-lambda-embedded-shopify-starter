import { shopIsInstalled } from './shopIsInstalled'
import { requestOriginatesFromShopify } from './requestOriginatesFromShopify'

export const validateRequest = async (queryString: any, shopifySecret: string) => {

  const { shop: shopName } = queryString;
  if (shopName && !await shopIsInstalled({ shopName })) {
    return 'NOT_INSTALLED';
  }

  if (await requestOriginatesFromShopify(queryString, shopifySecret) && await shopIsInstalled({ shopName })) {
    return 'AUTHORIZED';
  } else {
    return;
  }
}
