// import { docClient } from '../../aws/docClient'
import dynamodb from 'serverless-dynamodb-client'

const docClient = dynamodb.doc;

// Bring your own here
type ShopifyShop= {
  shopName: string;
  shopId: number;
  accessToken: string;
}

export const saveShopToDatabase = async ({ shopName, shopId, accessToken}: ShopifyShop) => {
  const params = {
    TableName: "ShopifyUsers",
    Item: {
      shopName,
      shopId,
      accessToken
    }
  }

  return await docClient.put(params).promise();
}
