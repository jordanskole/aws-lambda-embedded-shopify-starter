type TokenOptions = {
  shop: string;
  shopifyKey: string;
  shopifySecret: string;
  code: string;
}

export const getPermenantAuthToken = async ({ shop, shopifyKey, shopifySecret, code }: TokenOptions) => {
  const endpoint = `https://${shop}/admin/oauth/access_token`
  return await fetch(endpoint, {
    method: 'POST',
    headers: {

    },
    body: JSON.stringify({
      client_id: shopifyKey,
      client_secret: shopifySecret,
      code,
    })
  })
}
