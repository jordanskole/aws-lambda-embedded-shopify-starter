export const redirectTo = (url: string) => {
  return {
    statusCode: 302,
    headers: {
      "Location": url
    },
    body: null
  }
}
