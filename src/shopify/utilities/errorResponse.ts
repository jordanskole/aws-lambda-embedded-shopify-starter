// type ErrorResponse = {
//   statusCode: number;
//   body: string;
// }

export const errorResponse = (statusCode: number, error: Error): any => ({
  statusCode,
  body: JSON.stringify({
    name: error.name,
    message: error.message
  })
})
