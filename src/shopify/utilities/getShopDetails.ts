
type args = {
  shopName: string;
  accessToken: string;
}

export const getShopDetails = async ({ shopName, accessToken }: args) => {
  const res = await fetch(`https://${shopName}/admin/shop.json`, { headers: {'X-Shopify-Access-Token': accessToken}});
  return await res.json();
}
