type RegisterShopifyWebhook = {
  shop: string;
  webhook: string;
  token: string;
}

export const registerWebhook = async ({ shop, token, webhook }: RegisterShopifyWebhook) => {
  const payload = {
    "webhook": {
      "topic": webhook,
      "address": shop,
      "format": "json"
    }
  }
  return await fetch(`${shop}/admin/webhooks.json`, {
    method: 'POST',
    headers: {
      'X-Shopify-Access-Token:': token,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  });
}
