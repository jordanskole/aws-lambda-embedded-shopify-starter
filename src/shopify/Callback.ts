import { Context, Callback as CallbackType } from 'aws-lambda'
import {
  errorResponse,
  exchangeCodeForPermenantToken,
  getShopDetails,
  redirectTo,
  requestOriginatesFromShopify,
  saveShopToDatabase
} from './utilities'

const { APP_SLUG, SHOPIFY_SECRET } = process.env;



const Callback = async (event: any, context: Context, callback: CallbackType) => {
  context.callbackWaitsForEmptyEventLoop = false;

  try {
    const queryString = event.queryStringParameters;
    const { shop: shopName, code } = queryString;

    // validate the request came from shopify
    if (requestOriginatesFromShopify(queryString, SHOPIFY_SECRET)) {

      // exchange for an accessToken
      const { access_token: accessToken } = await exchangeCodeForPermenantToken({ shopName, code });

      // get shop details
      const { shop: { id: shopId } } = await getShopDetails({ shopName, accessToken });

      // save the shop to the db
      const response = await saveShopToDatabase({ shopName, shopId, accessToken });

      // register webhooks
      // redirect to the the app
      callback(null, redirectTo(`https://${shopName}/admin/apps/${APP_SLUG}`));
    } else {
      callback(errorResponse(400, new Error('Unauthorized request')), null)
    }
  } catch (err) {
    callback(errorResponse(500, err), null)
  }
}

export { Callback }
