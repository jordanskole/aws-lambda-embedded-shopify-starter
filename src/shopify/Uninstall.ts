import { Context, Callback } from 'aws-lambda'
import { verifyShopifyWebhook } from './utilities'

const Uninstall = (event: any, context: Context, callback: Callback) => {
  try {
    verifyShopifyWebhook(event.headers)
  } catch {
  }
}

export { Uninstall }
