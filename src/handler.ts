import { Context } from 'aws-lambda';
import { ApolloServer, defaultPlaygroundOptions } from 'apollo-server-lambda';
import { schema as typeDefs } from './schema';
import { resolvers } from './resolvers';

const server = new ApolloServer({
  typeDefs,
  resolvers,
  playground: defaultPlaygroundOptions,
  context: ({ event, context }: { event: any, context: Context}) => ({
    headers: event.headers,
    functionName: context.functionName,
    event,
    context,
  }),
});

const graphql = server.createHandler({
  cors: {
    origin: true,
    credentials: true,
  },
});


export { graphql }
