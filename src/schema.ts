import { gql } from 'apollo-server-lambda'

// Construct a schema, using GraphQL schema language
const schema = gql`
  type Query {
    hello: String
  }
`;

export { schema }
